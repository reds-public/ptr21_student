#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#include "io_utils.h"

static const unsigned char hex_digits[10] = {
    0x40, // 0
    0xf9, // 1
    0x24, // 2
    0x30, // 3
    0x19, // 4
    0x12, // 5
    0x02, // 6
    0x78, // 7
    0x00, // 8
    0x10, // 9
};

uint32_t keys(void *ioctrl)
{
    return *((uint32_t*)(ioctrl + 0x90));
}

uint32_t switches(void *ioctrl)
{
    return *((uint32_t*)(ioctrl + 0xa0));
}

uint32_t get_leds(void *ioctrl)
{
    return *((uint32_t*)(ioctrl + 0x80));
}

void set_leds(void *ioctrl, uint32_t leds)
{
    *((uint32_t*)(ioctrl + 0x80)) = leds;
}

void set_display_value(void *ioctrl, int value)
{
    *((uint32_t*)(ioctrl + 0x70)) = hex_digits[value/100000];
    value = value % 100000;
    *((uint32_t*)(ioctrl + 0x60)) = hex_digits[value/10000];
    value = value % 10000;
    *((uint32_t*)(ioctrl + 0x50)) = hex_digits[value/1000];
    value = value % 1000;
    *((uint32_t*)(ioctrl + 0x40)) = hex_digits[value/100];
    value = value % 100;
    *((uint32_t*)(ioctrl + 0x30)) = hex_digits[value/10];
    value = value % 10;
    *((uint32_t*)(ioctrl + 0x20)) = hex_digits[value];
}
