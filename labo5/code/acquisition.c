#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <cobalt/stdio.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/event.h>

#include "board.h"
#include "audio_utils.h"

struct write_task_args {
    data_t *samples_buf;
    size_t samples_buf_size;
};

static RT_TASK write_rt_task;

void write_task(void *cookie)
{
    struct write_task_args *args = (struct write_task_args*)cookie;

    data_t *samples_buf = args->samples_buf;
    int output_file;
    char output_filename[256] = "out.wav";
    size_t actually_read;
    size_t to_read = args->samples_buf_size;

    output_file = open(output_filename, O_CREAT | O_RDWR);
    if (output_file < 0) {
        perror("Could not open the output file");
        exit(EXIT_FAILURE);
    }

    if (write_wav_header(output_file)) {
        exit(EXIT_FAILURE);
    }
    rt_printf("Start recording in file %s\n", output_filename);

    while (1) {
        actually_read = read_samples(samples_buf, to_read);
        if (actually_read == 0) continue;
        if (append_wav_data(output_file, samples_buf, actually_read)) {
            rt_printf("Couldn't append data in WAV file");
            exit(EXIT_FAILURE);
        }
    }
}

int
main(int argc, char *argv[])
{
    struct write_task_args write_args;

    if (argc < 2) {
        printf("Not enough arguments. \
                Expected %s <freqTask>.\n", argv[0]);
        return EXIT_SUCCESS;
    }

    if (init_board() != 0) {
        perror("Error at board initialization.");
        exit(EXIT_FAILURE);
    }

    write_args.samples_buf = malloc(sizeof(data_t) * 1024);
    write_args.samples_buf_size = 1024;

    mlockall(MCL_CURRENT | MCL_FUTURE);

    if (rt_task_spawn(&write_rt_task, "write task", 0, 50, T_JOINABLE, write_task, &write_args) != 0) {
        perror("Error while starting write_task");
        exit(EXIT_FAILURE);
    }
    rt_task_join(&write_rt_task);

    clean_board();
    munlockall();

    return EXIT_SUCCESS;
}
