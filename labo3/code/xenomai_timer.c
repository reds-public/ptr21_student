#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/mman.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>

void xenomai_timer(void *arg)
{
    /* ... */
}

int main(int argc, char *argv[])
{
    /* ... */

    return EXIT_SUCCESS;
}
