#ifndef BOARD_H
#define BOARD_H

#include <unistd.h>
#include <stdint.h>

/* Bit offsets for keys */
#define KEY0    0x1
#define KEY1    0x2
#define KEY2    0x4
#define KEY3    0x8

/* Bit offsets for switches */
#define SW0     0x1

/* LED states */
#define LED_HIGH 0x1
#define LED_LOW  0x0

#define LED0	0
#define LED1	1
#define LED2	2
#define LED3	3
#define LED4	4
#define LED5	5
#define LED6	6
#define LED7	7
#define LED8	8
#define LED9	9

/* Sample data type and width */
typedef uint16_t data_t;
#define data_width          2 // int bytes

/**
 * @brief Initializes the board
 * @return 0 if everything went well, another number else.
 *
 * This function should be called at the beginning of the main(),
 * before any access is made to the various peripherals
 */
int init_board();

/**
 * @brief Cleans the board resources
 * @return 0 if everything went well, another number else.
 *
 * This function should be called before leaving the process.
 */
int clean_board(void);

/**
 * @brief Reads samples from a source
 * @param buf A pointer to a buffer receiving the samples
 * @param count The number of samples to receive
 * @return The actual number of samples read
 */
ssize_t read_samples(data_t *buf, size_t count);

/**
 * @brief Gets the status of buttons
 * @return The status of the 4 buttons.
 *
 * 1 means the button is pressed
 */
uint32_t get_buttons(void);

/**
 * @brief Gets the status of the switches
 * @return The status of the switches
 */
uint32_t get_switches(void);

/**
 * @brief Checks if a button was just pressed
 * @param buttons_before The previous value of the buttons
 * @param buttons_now The current value of the buttons
 * @param bit The button of interest
 * @return 1 if the button was just pressed, 0 else
 */
int is_pressed_now(uint32_t buttons_before, uint32_t buttons_now, uint8_t bit);

/**
 * @brief Sets the state of an led
 * @param the led number
 * @param the state of an led (LED_LOW/LED_HIGH)
 */
void set_led(uint8_t led_num, uint8_t state);

/**
 * @brief Sets the value displayed on hex leds
 * @param the value to display
 */
void display_value(int value);

#endif // BOARD_H
