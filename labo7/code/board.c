#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdbool.h>

#include <cobalt/stdio.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/mutex.h>

#include "io_utils.h"
#include "board.h"
#include "busycpu.h"

static int snd_fd;
static int rtsnd_fd;
static void *ioctrls;

/**
 * The overloading task is running with a period of 10ms, and will generate a CPU consumption of 50% 
 * when active. The activation is checked thanks to the variable overloading.
 * Its priority is 90. It is high, but allows other tasks to take correcting actions if required.
 */
#define OVERLOADING_PERIOD      ((RTIME)(10000000UL))
// #define OVERLOADING_TIME        ((RTIME)(4UL)) // Overload per period, in ms
static bool overloading = false; // Indicates if an overloading shall be generated or not
static RT_TASK overloading_generator_rt_task;

static RTIME overload_value = ((RTIME)4UL);
void set_overload_value(int value)
{
    if (value < 0) {
        printf("Error with the value of set_overload_value.");
        exit(1);
    }
    if (value > 100) {
        printf("Error with the value of set_overload_value.");
        exit(1);
    }
    overload_value = ((RTIME)value);
}

static void check_io() {
    uint32_t switches = get_switches();
    overloading = switches & SW0;
}

static void overloading_generator_task(void *cookie)
{
    int ret = rt_task_set_periodic(&overloading_generator_rt_task, TM_NOW, OVERLOADING_PERIOD);
    if (ret != 0) {
        printf("Error with setting the period of the overloading generator.");
        exit(1);
    }
    while (1) {
        check_io();

        if (overloading) {
            busy_cpu(overload_value * (OVERLOADING_PERIOD / 100));
            //busy_cpu(9200000);
        }

        rt_task_wait_period(NULL);
    }
}

int init_board()
{
    /* Ouverture du driver RTDM */
    rtsnd_fd = open("/dev/rtdm/snd", O_RDWR);
    if (rtsnd_fd < 0) {
        perror("Opening /dev/rtdm/snd");
        exit(EXIT_FAILURE);
    }

    snd_fd = rtsnd_fd;

    ioctrls = mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, rtsnd_fd, 0);
    if (ioctrls == MAP_FAILED) {
        perror("Mapping real-time sound file descriptor");
        exit(EXIT_FAILURE);
    }

    rt_task_spawn(&overloading_generator_rt_task, "overloading generator", 0, 90, T_JOINABLE, overloading_generator_task, NULL);

    return 0;
}

int clean_board(void)
{
    close(rtsnd_fd);
    if (munmap(ioctrls, 4096) == -1) {
        perror("Unmapping");
        exit(EXIT_FAILURE);
    }
    return 0;
}

ssize_t write_samples(const void *buf, size_t count)
{
    return write(snd_fd, buf, count);
}

ssize_t read_samples(data_t *buf, size_t count)
{
    return read(snd_fd, buf, count);
}

uint32_t get_buttons(void)
{
    return ~keys(ioctrls);
}

uint32_t get_switches(void)
{
    return switches(ioctrls);
}

int is_pressed_now(uint32_t buttons_before, uint32_t buttons_now, uint8_t bit)
{
    uint32_t before_bit = buttons_before & bit;
    uint32_t now_bit = buttons_now & bit;

    if (!now_bit && before_bit) return 1;
    else return 0;
}

void set_led(uint8_t led_num, uint8_t state)
{
    uint32_t current = get_leds(ioctrls);
    uint32_t after = (current & ~(1 << led_num)) | (state << led_num);

    set_leds(ioctrls, after);
}

void display_value(int value)
{
    set_display_value(ioctrls, value);
}

